from typing import List, Tuple

from PIL import Image, ImageOps, ImageDraw, ImageDraw2

Transform = Tuple[int, int, int]
Vec2i = Tuple[int, int]
PathSegment = Tuple[Vec2i, Transform]


def peano_layout():
    """ 
    Peano curve segment layout.
    Basically a set of moves and segment transforms you need to do to get 
    from top left corner to other corner, while not breaking a curve.

    See method `tf` for specification of transforms.
    """
    # (offx, offy), (rotate90, flipX, flipY)
    return [
        ((0, 1), (0, 1, 0)),
        ((0, 1), (0, 1, 0)),
        ((1, 0), (0, 0, 1)),
        ((0, -1), (0, 1, 0)),
        ((0, -1), (0, 1, 0)),
        ((1, 0), (0, 0, 1)),
        ((0, 1), (0, 1, 0)),
        ((0, 1), (0, 1, 0)),
    ]


def v2i_sum(a: Vec2i, b: Vec2i):
    """ Returns sum of two vectors """
    return a[0] + b[0], a[1] + b[1]


def v2i_tf(p: Vec2i, transform: Transform):
    """ Transforms just like in function `tf`. """
    r90, flip_x, flip_y = transform
    if r90:
        rm = [
            [(0, 1), (1, 1)],
            [(0, 0), (1, 0)]
        ]
        p = rm[p[1]][p[0]]
    if flip_x:
        p = (0 if p[0] else 1, p[1])
    if flip_y:
        p = (p[0], 0 if p[1] else 1)
    return p


def v2i_scl(p: Vec2i, scl: Vec2i):
    """ Scales given vector by another one """
    return p[0] * scl[0], p[1] * scl[1]


def v2i_mid(a: Vec2i, b: Vec2i):
    """ Returns average between two given vectors """
    return (a[0] + b[0]) // 2, (a[1] + b[1]) // 2


def tf(img: Image.Image, transform: Transform):
    """ 
    Transforms images with given transform tuple. 
    Transform tuple -> (rotate90, flip_x, flip_y), 
    where each element can be either 1 or 0.
    """
    r90, flip_x, flip_y = transform

    if r90:
        img = img.transpose(Image.ROTATE_90)
    if flip_x:
        img = ImageOps.mirror(img)
    if flip_y:
        img = ImageOps.flip(img)

    return img


def calc_segment_size(layout: List[PathSegment]):
    """ Calculates size of the segment, in subsegments. """
    pix = 0, 0
    mx = 0
    my = 0
    for path, _ in layout:
        pix = v2i_sum(pix, path)
        mx = max(mx, pix[0])
        my = max(my, pix[1])
    return mx + 1, my + 1


def segment_exit(layout: List[PathSegment]):
    """ Calculates segment exit. """
    size = calc_segment_size(layout)
    pos = 1, 1
    for p, _ in layout:
        pos = v2i_sum(p, pos)

    return 1 if pos[0] == size[0] else 0, 1 if pos[1] == size[1] else 0


def darken(img: Image.Image):
    """
    Makes image a bit darker.
    """
    cp = img.copy()
    cp.putdata(
        list(
            (int(b / 1.5), int(b / 1.5), int(b / 1.5))
            for r, g, b
            in cp.getdata()
        )
    )
    return cp


def generate(order: int, layout: List[PathSegment]):
    """
    Creates self-similar curve of order `order` from given layout. 
    See `peano_layout` for an example.
    """
    if order == 1:
        img = Image.new("RGB", (1, 1), "#000000")
    else:

        segment = generate(order - 1, layout)
        lw, lh = calc_segment_size(layout)
        pw, ph = segment.size

        def l(x: int, y: int):
            return layout[y][x]

        def b(x: int, y: int):
            x_ = x * pw + x - 1
            y_ = y * ph + y - 1
            return x_, y_  # , x_ + pw, y + ph

        img = Image.new(
            "RGB",
            (
                pw * lw + lw - 1,
                ph * lh + lh - 1
            ),
            "#FFFFFF"
        )
        pix = 0, 0
        pos = 0, 0
        enter = 0, 0
        exit = segment_exit(layout)
        scale = v2i_sum(segment.size, (-1, -1))

        img.paste(segment, (0, 0))

        for off, transform in layout:
            pix = v2i_sum(off, pix)

            segment = tf(segment, transform)

            prev_exit = exit

            enter = v2i_tf(enter, transform)
            exit = v2i_tf(exit, transform)

            x, y = pix

            prev_pos = pos
            pos = (x * segment.width + x, y * segment.height + y)

            tmp = segment if (x + y) % 2 == 0 else darken(segment)
            img.paste(tmp, pos)

            img.putpixel(
                v2i_mid(
                    v2i_sum(
                        pos,
                        v2i_scl(enter, scale)
                    ),
                    v2i_sum(
                        prev_pos,
                        v2i_scl(prev_exit, scale)
                    )
                ),
                (0, 0, 0))

    return img


peano = generate(8, peano_layout())
peano.save("peano.png")
