# Self-similar curve generator

For now I am using this to create peano curve for my avatar.

It only supports integer curves, and does not fill gaps with lines (it does that with pixels), but that can be easily patched.
